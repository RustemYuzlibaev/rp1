class Blockchain {
    static blockchain = [];

    static init({ genesisBlock }) {
        if (Blockchain.blockchain.length === 0) {
            this.blockchain.push(genesisBlock);
        }
    }

    static getLastBlock() {
        return this.blockchain[this.blockchain.length - 1];
    }

    static addBlock(newBlock) {
        if (Blockchain.checkBlock(newBlock, this.getLastBlock())) {
            this.blockchain.push(newBlock);
        }
    }

    static checkBlock(newBlock, prevBlock) {
        const blockHash = newBlock.toHash();

        if (prevBlock.index + 1 !== newBlock.index) {
            console.error('Invalid index');
            return false;
        }

        if (prevBlock.hash !== newBlock.previousHash) {
            console.error('Invalid previous hash');
            return false;
        }

        if (blockHash !== newBlock.hash) {
            console.error('Invalid block hash');
            return false;
        }

        return true;
    }

    static getBlockchain() {
        return this.blockchain;
    }
}

module.exports = Blockchain;
