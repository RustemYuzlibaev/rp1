const { v4: uuidv4 } = require('uuid');

class SmartContract {
    constructor(participants, details, conditions) {
        this.id = uuidv4();
        this.observers = participants.observers;
        this.customer = participants.customer;
        this.contractor = participants.contractor;
        this.details = details;
        this.conditions = conditions;
    }

    toBinary() {
        const reward = this.observers.map(obs => {
            return `10$ is paid to ${obs.publicKey}`;
        });

        console.log(typeof reward);

        const payload = JSON.stringify(
            'Customer transaction\n' +
                this.details.price +
                '$' +
                ' is paid to this account: ' +
                this.contractor.publicKey +
                '\nRewards:\n' +
                reward
        );

        const script = `console.log(${payload});`;

        return Buffer.from(script);
    }
}

module.exports = SmartContract;
