// first demo

const Blockchain = require('./Blockchain');
const Block = require('./Block');

// init process
Blockchain.init({ genesisBlock: Block.genesis });

setInterval(() => {
    const newBlock = {
        index: Blockchain.getLastBlock().index + 1,
        previousHash: Blockchain.getLastBlock().hash,
        transactions: [],
        contract: {},
    };

    const block = new Block(newBlock);
    Blockchain.addBlock(block);

    // print
    console.log(Blockchain.getBlockchain());

    console.log('\n\n\n\n\n\n\n');
}, 3000);
