// second demo

const fs = require('fs');
const { exec } = require('child_process');
const crypto = require('crypto');

const Blockchain = require('./Blockchain');
const Block = require('./Block');
const SmartContract = require('./contract/SmartContract');
const Observer = require('./players/Observer');
const Contractor = require('./players/Contractor');
const Customer = require('./players/Customer');

const generateKeys = require('./util/generateKeyPair');

// create observer
async function ObserverGenerator() {
    const { publicKey, privateKey } = await generateKeys();

    const observer = new Observer({
        publicKey,
        privateKey,
    });

    return observer;
}

// create customer
async function CustomerGenerator() {
    const { publicKey, privateKey } = await generateKeys();

    const customer = new Customer({
        publicKey,
        privateKey,
    });

    return customer;
}

// create contractor
async function ContractorGenerator() {
    const { publicKey, privateKey } = await generateKeys();

    const contractor = new Contractor({
        publicKey,
        privateKey,
    });

    return contractor;
}

(async () => {
    const observer1 = await ObserverGenerator();
    const observer2 = await ObserverGenerator();
    const observer3 = await ObserverGenerator();

    const customer1 = await CustomerGenerator();

    const contractor1 = await ContractorGenerator();

    // *** hardcoded contract ***
    players = {
        observers: [observer1, observer2, observer3],
        customer: customer1,
        contractor: contractor1,
    };

    details = {
        price: '500',
        type: 'service',
        description: 'Carry out some goods in Kazan (30km)',
    };

    conditions = {
        notAfter: new Date('August 19, 2021').toString(),
    };

    const contract = new SmartContract(players, details, conditions);

    observer1.addDeal(contract.id);
    const sign = crypto.createSign('RSA-SHA256');
    sign.update('Observer1 signed');
    sign.end();
    const signature = sign.sign(observer1.privateKey);

    observer2.addDeal(contract.id);
    observer3.addDeal(contract.id);

    // init process
    Blockchain.init({ genesisBlock: Block.genesis });

    const newBlock = {
        index: Blockchain.getLastBlock().index + 1,
        previousHash: Blockchain.getLastBlock().hash,
        transactions: [
            observer1.publicKey,
            observer2.publicKey,
            observer3.publicKey,
        ],
        contract: { id: contract.id, binary: contract.toBinary() },
    };

    const block = new Block(newBlock);

    Blockchain.addBlock(block);

    const id = Blockchain.getBlockchain()[1].contract.id;
    const script = Blockchain.getBlockchain()[1].contract.binary.toString();

    observer1.confirmDeal(contract.id);
    const verify = crypto.createVerify('SHA256');
    verify.update('Observer1 signed');
    verify.end();

    if (verify.verify(observer1.publicKey, signature)) {
        fs.writeFile(`${id}.js`, script, err => {
            if (err) throw err;
            console.log('The transaction has been created.\n');
        });
        exec(`node ${id}.js`, (error, stdout) => {
            if (error) {
                console.error(`exec error: ${error}`);
                return;
            }
            console.log(`${stdout}`);
        });
    }
})();
