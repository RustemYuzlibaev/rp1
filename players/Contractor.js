class Contractor {
    constructor({ publicKey, privateKey }) {
        this.publicKey = publicKey;
        this.privateKey = privateKey;
    }
}

module.exports = Contractor;
