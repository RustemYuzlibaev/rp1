class Customer {
    constructor({ publicKey, privateKey }) {
        this.publicKey = publicKey;
        this.privateKey = privateKey;
    }
}

module.exports = Customer;
