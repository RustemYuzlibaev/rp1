class Observer {
    constructor({ publicKey, privateKey }) {
        this.publicKey = publicKey;
        this.privateKey = privateKey;
        this.deals = [];
    }

    addDeal(smartContractID) {
        this.deals.push({ [smartContractID]: false });
    }

    confirmDeal(smartContractID) {
        const result = this.deals.find((deal, index, array) => {
            if (Object.getOwnPropertyNames(deal)[0] === smartContractID) {
                array[index][smartContractID] = true;
            }
        });
    }
}

module.exports = Observer;
