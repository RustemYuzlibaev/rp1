const crypto = require('crypto');
const sign = crypto.createSign('RSA-SHA256');
const verify = crypto.createVerify('SHA256');
const generateKeys = require('../util/generateKeyPair');

class Node {
    static async getNode() {
        return await Node.getKeyPair();
    }

    static async getKeyPair() {
        const { publicKey, privateKey } = await generateKeys();
        return { publicKey, privateKey };
    }
}

async function test() {
    try {
        const Node1 = await Node.getNode();
        const Node2 = await Node.getNode();

        sign.update('Alice sent Bob 20rub', 'hex');
        const signature = sign.sign(Node1.privateKey);
        console.log(signature);

        const result = verify.verify(Node1.publicKey, signature);
        console.log(result);
    } catch (error) {
        console.error(error);
    }
}

test();
