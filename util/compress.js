const util = require('util');
const { gzip, unzip } = require('zlib');

const compress = util.promisify(gzip);
const decompress = util.promisify(unzip);

async function comressData(data) {
    return await compress(data);
}

async function decompressData(data) {
    return await decompress(data);
}

module.exports = { comressData, decompressData };
