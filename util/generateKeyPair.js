const util = require('util');
const { generateKeyPair } = require('crypto');

const generate = util.promisify(generateKeyPair);

async function generateKeys() {
    return await generate('rsa', {
        modulusLength: 512,
        publicKeyEncoding: {
            type: 'spki',
            format: 'pem',
        },
        privateKeyEncoding: {
            type: 'pkcs8',
            format: 'pem',
        },
    });
}

module.exports = generateKeys;
