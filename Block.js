const crypto = require('crypto');

const Blockchain = require('./Blockchain');

class Block {
    constructor({ index, previousHash, transactions, contract }) {
        this.index = index;
        this.previousHash = previousHash;
        this.timestamp = Date.now();
        this.transactions = transactions;
        this.contract = contract;
        this.nonce = crypto.randomBytes(16).toString('hex');
        this.hash = this.toHash();
    }

    toHash() {
        return crypto
            .createHash('sha256')
            .update(
                (
                    this.index +
                    this.previousHash +
                    this.timestamp +
                    this.transactions +
                    this.contracts +
                    this.nonce
                ).toString()
            )
            .digest('hex');
    }

    static get genesis() {
        return new Block({
            index: 0,
            previousHash: 0,
            timestamp: Date.now(),
            transactions: [],
            contract: {},
            nonce: 0,
        });
    }
}

module.exports = Block;
